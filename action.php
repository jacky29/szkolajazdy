<?php
header('Content-Type: application/json');
$rest_json = file_get_contents("php://input");
$_POST = json_decode($rest_json, true);
if(isset($_POST)) {
 
	$email_to = "zapisy.mbsswe@gmail.com";
	#$email_to = "jacekdwulit@gmail.com";
    $email_subject = "Formularz Zgloszeniowy";
 
    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
 
    }
 
    $firstName = $_POST['name'];
    $lastName = $_POST['school'];
    $email_from = $_POST['email'];
    $telephone = $_POST['phone'];
	$player1 = $_POST['player1'];
	$player2 = $_POST['player2'];
	$player3 = $_POST['player3'];
	$player4 = $_POST['player4'];
	$player5 = $_POST['player5'];
	$player6 = $_POST['player6'];
	$additionalMessage = $_POST['message'];
	$game = $_POST['game'];
	$curr_timestamp = date('Y-m-d H:i:s');
	
    $email_message = "Zgloszenie do turnieju eSport.\n";
 
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
    
    $email_message .= "\n(===||:::::::::::::::>\n\n";
 
    $email_message .= "Zglaszajacy: ".clean_string($firstName)."\n";
    $email_message .= "Szkola: ".clean_string($lastName)."\n";
 
    $email_message .= "Email: ".clean_string($email_from)."\n";
    $email_message .= "Telefon: ".clean_string($telephone)."\n";
    $email_message .= "Gra: ".clean_string($game)."\n\n";
 
	$email_message .= "\n\n(===||:::::::::::::::>\n\n";
 
    $email_message .= "Uczestnicy\n";
    $email_message .= "Gracz 1/Kapitan: " .clean_string($player1). "\n";
    $email_message .= "Gracz 2: " .clean_string($player2). "\n";
	$email_message .= "Gracz 3: " .clean_string($player3). "\n";
	$email_message .= "Gracz 4: " .clean_string($player4). "\n";
	$email_message .= "Gracz 5: " .clean_string($player5). "\n";
	$email_message .= "Gracz 6/Rezerwowy: " .clean_string($player6). "\n\n";
	
	$email_message .= clean_string($additionalMessage);
	
	$email_message .= "\n\nZgloszene zostalo wyslane ze strony internetowej dnia:\n" .$curr_timestamp;
 
// create email headers
 
$headers = 'Od: '.$email_from."\r\n".
 
'Reply-To: '.$email_to."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
mail($email_to, $email_subject, $email_message, $headers); 
 
?>
{
    <!-- console degugger check to see if the form submitted correctly -->
    "success": true,
} 
 
<?php
 
} else {
?>   
{
    "success": false
} 

<?php
}
 
?>